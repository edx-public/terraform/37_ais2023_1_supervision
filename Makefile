.PHONY: one all clean

one:
	@./make-all.sh gr1

all:
	@./make-all.sh

clean:
	@./clean-all.sh
