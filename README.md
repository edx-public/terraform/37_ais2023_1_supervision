# 37_AIS2024_1_Supervision

Ce dépôt Terraform contient les configurations nécessaires pour déployer cinq infrastructures isolées sur AWS, destinées à des travaux pratiques en réseau pour cinq groupes d'étudiants. Chaque environnement est sécurisé et cloisonné, inaccessible depuis l'extérieur afin de prévenir toute exposition non désirée. La segmentation du réseau est réalisée à travers l'implémentation de multiples VLANs, assurant ainsi une séparation claire et sécurisée des ressources pour chaque groupe. L'accès à ces environnements est exclusivement réservé aux utilisateurs authentifiés par OpenVPN, avec l'authentification renforcée par des certificats. Cette stratégie de sécurité garantit un cadre de travail sûr pour les étudiants, tout en leur permettant de manipuler une infrastructure complexe dans un contexte contrôlé.

## Disclaimer

Ce dépôt est mis à disposition exclusivement à des fins pédagogiques et de formation. Le code présenté ici n'est pas optimisé, une décision délibérée dictée par des contraintes de temps et surtout par un objectif d'enseignement. Les étudiants sont encouragés à utiliser les principes de l'Open Source Intelligence (OSINT) pour rechercher, examiner et comprendre le code fourni. Ce processus vise à enrichir leur expérience d'apprentissage en leur permettant de découvrir et de résoudre des problèmes de manière autonome.

Il est important de noter que, bien que le code soit fonctionnel, il pourrait être amélioré en termes d'efficacité et de pratiques de codage. Les contributeurs et utilisateurs doivent traiter ce dépôt comme un terrain de jeu éducatif, permettant aux étudiants de se familiariser avec les outils et méthodologies essentiels en matière de sécurité réseau et de déploiement sur le cloud.

## Licence

Ce projet est diffusé sous la Licence Creative Commons Attribution Share Alike 4.0 International (CC BY-SA 4.0). Pour voir une copie de cette licence, visitez [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) ou envoyez une lettre à Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

En vertu de cette licence, vous êtes libre de :

- **Partager** — copier et redistribuer le matériel sur tout support ou format
- **Adapter** — remixer, transformer et construire à partir du matériel pour toute utilisation, y compris commerciale.

Cette licence est acceptable pour les œuvres culturelles libres.

Sous les conditions suivantes :

- **Attribution** — Vous devez donner le crédit approprié, fournir un lien vers la licence et indiquer si des modifications ont été effectuées. Vous pouvez le faire de manière raisonnable, mais pas d'une manière qui suggérerait que le concédant vous soutient ou soutient la façon dont vous utilisez son œuvre.
- **ShareAlike** — Si vous remixer, transformez ou créez à partir du matériel, vous devez diffuser vos contributions sous la même licence que l'original.

Pas de restrictions supplémentaires — Vous ne pouvez pas appliquer des conditions juridiques ou des mesures technologiques qui restreignent légalement les autres à faire tout ce que la licence permet.

# Implementation

## Mise en route de l'infrastructure

- Modifier le fichier `main.private.tfvars` pour y mettre vos credentials AWS.
- La Région est définie dans le fichier : `main.tf`.
❯ aws ec2 create-key-pair --key-name KeyPairAISTeacher --query "KeyMaterial" --output text > key_pairs/KeyPairAISTeacher.pem
- lancer `make all` pour instancier toute l'infrastructure ou `make one` pour lancer qu'une infrastructure du Groupe 1.

## Démontage de l'infrastructure

- lancer `make clean`

## Acces aux machines

- Les **Key Pairs** sont fournis : Ce n'est pas Secure - Mais c'est pour de la formation
- Les configurations OpenVPN se crées automatiquement dans le répertoire : `ovpn_configs`. 5 Clients par Groupe et 5 Groupes.
- Les Archives étudiants des configurations VPN sont dans le répertoire `clients_ovpn_configs`.
- Les VMs : instance-2-{3..7} avec les adresses IP : _192.168.10?.{101..105}_ sont des **ubuntu 22.04 LTS** (login : ubuntu) disponible pour les étudiants.
- Il y a 5 VMs particulières (1 par groupe) avec adresse IP dynamique qui sont là pour montrer la partie CloudWatch.
- Il y a 5 VMs _192.168.10?.52_. sont là pour le formateur si il doit intervenir ou mettre de nouvelles fonctionnalités pendant la séquence.

## Actions post Terraformation !

Pour des raisons de temps, certaines actions, n'ont pas été automatisé. Elles sont peut nombreuses et listées ci-dessous.

- [ ] Autoriser dans pi.hole toutes les sources
- [ ] Instancier T-Pot
- [ ] Installation Netdata sur Fedora CoreOS

## Temps de terraformation

- `make one`  30,56s user 5,10s system 3% cpu 14:59,13 total
- `make clean`  52,28s user 6,92s system 4% cpu 21:51,68 total
- `make all`  51,89s user 9,09s system 6% cpu 15:55,72 total

## Synchronisation Panneaux dans tmux

- Appuyez sur **Prefix (Ctrl-a/b)** puis tapez **:** pour entrer dans le mode commande de tmux.
- Dans le mode commande, tapez `setw synchronize-panes on` et appuyez sur __Entrée__.

Après avoir activé la synchronisation, tout ce que vous tapez dans un panneau sera reproduit dans tous les autres panneaux de la même fenêtre.
Désactiver la Synchronisation des Panneaux
pour désactiver cette fonctionnalité et permettre à nouveau à chaque panneau de fonctionner indépendamment, suivez ces étapes :

- Appuyez sur **Prefix (Ctrl-a/b)** puis tapez **:** pour entrer dans le mode commande.
- Tapez `setw synchronize-panes off` et appuyez sur __Entrée__.

## Installation Netdata sur Fedora CoreOS

```
$ podman run -d --name=netdata -p 19999:19999 -v /proc:/host/proc:ro -v /sys:/host/sys:ro \
        -v /var/run/docker.sock:/var/run/docker.sock:ro --cap-add SYS_PTRACE \
        --security-opt label=disable quay.io/netdata/netdata:latest
$ podman generate systemd --name netdata --files
$ cp container-netdata.service /etc/systemd/system
$ systemctl daemon-reload
$ systemctl enable container-netdata.service
$ systemctl start container-netdata.service

```
## OpenVPN - Accès à l'infrastructure

Compatible **GNU/Linux** et **Apple Silicon**.

```
$ cd ovpn_configs/
$ ../launch_ovpn.sh [all,config,client1-gr1,...]
```

## Infrastructure AWS

![AWS Infrastructure](./Screenshots/Screenshot_aws-infrastructure.png)
