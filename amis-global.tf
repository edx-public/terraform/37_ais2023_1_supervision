# aws ec2 describe-images --filters "Name=description,Values=*Zabbix*" --query 'Images[*].[ImageId,Name,Description]' --region eu-west-3
# watch -n 10 aws ec2 describe-instance-status --query "InstanceStatuses[*].[InstanceId,InstanceState.Name,SystemStatus.Status,InstanceStatus.Status]" --output table

variable "ami_instance1-1" {
    # https://cloud-images.ubuntu.com/locator/ec2/
    description = "AMI à utiliser pour Instance4 - AMD64 - Bookworm"
    type        = string
    default     = "ami-0da03322d375c0bc4"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance1-2" {
    # https://cloud-images.ubuntu.com/locator/ec2/
    description = "AMI à utiliser pour Instance4 - AMD64 - Bookworm"
    type        = string
    default     = "ami-0da03322d375c0bc4"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance1-3" {
    # https://wiki.debian.org/Cloud/AmazonEC2Image/Bullseye
    description = "AMI à utiliser pour Instance4 - AMD64 - Bullseye"
    type        = string
    default     = "ami-07d13c56c58f52fdd"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance1-4" {
    # https://bitnami.com/stack/wordpress/cloud/aws/amis
    description = "AMI à utiliser pour Instance1 - Bitnami - Wordpress"
    type        = string
    default     = "ami-070a2edade1dc70a1"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance1-5" {
    # https://cloud-images.ubuntu.com/locator/ec2/
    description = "AMI à utiliser pour Instance1 - Bitnami - Wordpress"
    type        = string
    default     = "ami-070a2edade1dc70a1"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance2-1" {
    # https://bitnami.com/stack/odoo/cloud/aws/amis
    description = "AMI à utiliser pour Instance2 - AMD64 - Bitnami - Odoo"
    type        = string
    default     = "ami-02a1a833bd7c05059"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance2-2" {
    # https://fedoraproject.org/en/coreos/download/?tab=cloud_launchable&stream=stable
    description = "AMI à utiliser pour Instance2 - AMD64 - Fedora CoreOS"
    type        = string
    default     = "ami-0f37cd4ceb06f944a"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance2-8" {
    # https://cloud-images.ubuntu.com/locator/ec2/
    description = "AMI à utiliser pour Instance4 - ARM - Ubuntu/22.04LTS"
    type        = string
    default     = "ami-0ebbdab89cca861f3"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance3-1" {
    # https://cloud-images.ubuntu.com/locator/ec2/
    description = "AMI à utiliser pour Instance4 - AMD64 - Bookworm"
    type        = string
    default     = "ami-0da03322d375c0bc4"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance3-2" {
    # https://bitnami.com/stack/gitlab/cloud/aws/amis
    description = "AMI à utiliser pour Instance3 - AMD64 - Bitnami - Gitlab-ce"
    type        = string
    default     = "ami-0668e570bcf659c67"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance3-3" {
    # https://bitnami.com/stack/gitlab/cloud/aws/amis
    description = "AMI à utiliser pour Instance4 - AMD64 - Bookworm"
    type        = string
    default     = "ami-0da03322d375c0bc4"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance3-4" {
    description = "AMI à utiliser pour Instance4 - Windows Server 2025"
    type        = string
    default     = "ami-0b221987f93a1dbfe"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance4-1" {
    # https://wiki.debian.org/Cloud/AmazonEC2Image/Bookworm
    description = "AMI à utiliser pour Instance4 - AMD64 - Bookworm"
    type        = string
    default     = "ami-0da03322d375c0bc4"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance4-2" {
    # https://wiki.debian.org/Cloud/AmazonEC2Image/Bookworm
    description = "AMI à utiliser pour Instance4 - AMD64 - Bookworm"
    type        = string
    default     = "ami-0da03322d375c0bc4"  # Remplacez ceci par l'ID de votre AMI préféré
}

variable "ami_instance_etudiant" {
    # https://cloud-images.ubuntu.com/locator/ec2/
    description = "AMI à utiliser pour Instance4 - AMD64 - Ubuntu/22.04LTS"
    type        = string
    default     = "ami-0966a535847b3d1ec"  # Remplacez ceci par l'ID de votre AMI préféré
}

