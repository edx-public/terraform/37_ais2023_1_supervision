resource "aws_iam_role" "backup_role" {
  name = "aws_backup_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          Service = "backup.amazonaws.com"
        },
        Effect = "Allow",
        Sid = ""
      },
    ]
  })
}

resource "aws_iam_policy" "backup_policy" {
  name   = "aws_backup_policy"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeVolumes",
        "ec2:DescribeSnapshots",
        "ec2:CreateTags",
        "ec2:CreateSnapshot",
        "ec2:DescribeTags",
        "ec2:DeleteSnapshot",
        "ec2:CreateImage",
        "ec2:DescribeInstances",
        "ec2:DescribeImages",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeInstanceCreditSpecifications",
        "ec2:DescribeInstanceAttribute",
        "tag:GetResources"
      ],
      "Resource": "*"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "backup_policy_attachment" {
  role       = aws_iam_role.backup_role.name
  policy_arn = aws_iam_policy.backup_policy.arn
}


# Coffre de sauvegarde AWS
resource "aws_backup_vault" "cefim_vault" {
  name = "cefim-vault"
}

# Plan de sauvegarde AWS
resource "aws_backup_plan" "cefim_backup_plan" {
  name = "cefim-backup-plan"

  rule {
    rule_name         = "cefim-backup-rule"
    target_vault_name = aws_backup_vault.cefim_vault.name
    schedule          = "cron(45 11,17 * * ? *)"
  }
}

# Sélection de sauvegarde AWS basée sur les balises
resource "aws_backup_selection" "cefim_selection" {
  plan_id = aws_backup_plan.cefim_backup_plan.id
  iam_role_arn = aws_iam_role.backup_role.arn
  selection_tag {
    type  = "STRINGEQUALS"
    key   = "Backup"
    value = "true"
  }
  name = "cefim-selection"
}

