#!/bin/bash

# Définir le chemin des fichiers Terraform (à modifier si nécessaire)
TF_PATH="."

# Activer l'affichage complet avec --all
SHOW_ALL=false
if [[ "$1" == "--all" ]]; then
    SHOW_ALL=true
fi

# Extraire les ressources Terraform
RESOURCES=$(grep -r 'resource "' $TF_PATH | awk '{print $2, $3}' | sed 's/"//g')

# Début de la vérification
echo "🔍 Vérification des ressources AWS existantes..."
echo "---------------------------------------------"

# Vérifier chaque ressource avec AWS CLI
while read -r TYPE NAME; do
    case $TYPE in
        aws_instance)
            INSTANCE_ID=$(aws ec2 describe-instances \
                --filters "Name=tag:Name,Values=$NAME" "Name=instance-state-name,Values=running,stopped" \
                --query 'Reservations[*].Instances[*].InstanceId' --output text | tr '\n' ' ')
            ;;
        aws_iam_role)
            INSTANCE_ID=$(aws iam get-role --role-name $NAME --query 'Role.RoleName' --output text 2>/dev/null)
            ;;
        aws_iam_policy)
            INSTANCE_ID=$(aws iam list-policies --query "Policies[?PolicyName=='$NAME'].PolicyName" --output text)
            ;;
        aws_eip)
            INSTANCE_ID=$(aws ec2 describe-addresses --query "Addresses[?Tag.Name=='$NAME'].PublicIp" --output text)
            ;;
        aws_s3_bucket)
            aws s3 ls "s3://$NAME" > /dev/null 2>&1 && INSTANCE_ID="EXISTE" || INSTANCE_ID=""
            ;;
        aws_backup_vault)
            INSTANCE_ID=$(aws backup list-backup-vaults --query "BackupVaultList[?BackupVaultName=='$NAME'].BackupVaultName" --output text)
            ;;
        aws_security_group)
            INSTANCE_ID=$(aws ec2 describe-security-groups --query "SecurityGroups[?GroupName=='$NAME'].GroupId" --output text)
            ;;
        aws_vpc)
            INSTANCE_ID=$(aws ec2 describe-vpcs --query "Vpcs[?Tags[?Key=='Name'&&Value=='$NAME']].VpcId" --output text)
            ;;
        aws_subnet)
            INSTANCE_ID=$(aws ec2 describe-subnets --query "Subnets[?Tags[?Key=='Name'&&Value=='$NAME']].SubnetId" --output text)
            ;;
        # Ressources non gérées
        aws_network_interface|aws_cloudwatch_log_group|aws_cloudwatch_dashboard|aws_iam_role_policy_attachment|aws_iam_instance_profile)
            if [[ "$SHOW_ALL" == true ]]; then
                echo "📌 Vérification de $TYPE $NAME ... ⚠ Ressource non supportée par le script. Vérifie manuellement."
            fi
            continue
            ;;
        *)
            if [[ "$SHOW_ALL" == true ]]; then
                echo "📌 Vérification de $TYPE $NAME ... ⚠ Ressource inconnue. Vérifie manuellement."
            fi
            continue
            ;;
    esac

    if [[ -z "$INSTANCE_ID" ]]; then
        if [[ "$SHOW_ALL" == true ]]; then
            echo "📌 Vérification de $TYPE $NAME ... ❌ NON TROUVÉ"
        fi
    else
        echo "📌 Vérification de $TYPE $NAME ... ✅ EXISTE ($INSTANCE_ID)"
    fi
done <<< "$RESOURCES"

echo "✅ Vérification terminée !"
