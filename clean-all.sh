#! /bin/bash

export TF_LOG=DEBUG
export TF_LOG_PATH=./terraform.log

terraform destroy -auto-approve -var-file="main.private.tfvars"
rm -f certificates.auto.tfvars
rm -fr pki/ ovpn_configs/
# Définition du domaine pour lequel les certificats doivent être supprimés
DOMAIN="aws.cefim.intra"

# Liste et supprime tous les certificats ACM pour le domaine spécifié
aws acm list-certificates --query "CertificateSummaryList[?contains(DomainName, '$DOMAIN')].[CertificateArn]" --output text | tr '\t' '\n' | while read arn; do
    echo "Suppression du certificat : $arn"
    aws acm delete-certificate --certificate-arn "$arn"
done

for group in {2..5}; do
    rm -f instance-?-?_gr${group}.tf
done

./delete-backups.sh

echo "Tous les certificats pour le domaine '$DOMAIN' ont été supprimés."
