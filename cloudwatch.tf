# Filtrer les instances EC2 à surveiller
data "aws_instances" "monitored_gr1" {
  instance_tags = {
    Monitored_gr1 = "true"
  }
}

data "aws_instances" "monitored_gr2" {
  instance_tags = {
    Monitored_gr2 = "true"
  }
}

data "aws_instances" "monitored_gr3" {
instance_tags = {
    Monitored_gr3 = "true"
  }
}

data "aws_instances" "monitored_gr4" {
instance_tags = {
    Monitored_gr4 = "true"
  }
}

data "aws_instances" "monitored_gr5" {
instance_tags = {
    Monitored_gr5 = "true"
  }
}

# Fonction pour générer des alarmes CloudWatch
locals {
  monitored_gr1_instances = toset(data.aws_instances.monitored_gr1.ids)
  metrics_gr1 = [
    { name = "CPUUtilization", namespace = "AWS/EC2", statistic = "Average", threshold = 80 },
    { name = "NetworkIn", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
    { name = "NetworkOut", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
  ]
}

# Fonction pour générer des alarmes CloudWatch
locals {
  monitored_gr2_instances = toset(data.aws_instances.monitored_gr2.ids)
  metrics_gr2 = [
    { name = "CPUUtilization", namespace = "AWS/EC2", statistic = "Average", threshold = 80 },
    { name = "NetworkIn", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
    { name = "NetworkOut", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
  ]
}

# Fonction pour générer des alarmes CloudWatch
locals {
  monitored_gr3_instances = toset(data.aws_instances.monitored_gr3.ids)
  metrics_gr3 = [
    { name = "CPUUtilization", namespace = "AWS/EC2", statistic = "Average", threshold = 80 },
    { name = "NetworkIn", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
    { name = "NetworkOut", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
  ]
}

# Fonction pour générer des alarmes CloudWatch
locals {
  monitored_gr4_instances = toset(data.aws_instances.monitored_gr4.ids)
  metrics_gr4 = [
    { name = "CPUUtilization", namespace = "AWS/EC2", statistic = "Average", threshold = 80 },
    { name = "NetworkIn", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
    { name = "NetworkOut", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
  ]
}

# Fonction pour générer des alarmes CloudWatch
locals {
  monitored_gr5_instances = toset(data.aws_instances.monitored_gr5.ids)
  metrics_gr5 = [
    { name = "CPUUtilization", namespace = "AWS/EC2", statistic = "Average", threshold = 80 },
    { name = "NetworkIn", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
    { name = "NetworkOut", namespace = "AWS/EC2", statistic = "Sum", threshold = 10000000 },
  ]
}

# Création des alarmes CloudWatch
resource "aws_cloudwatch_metric_alarm" "instance_alarms_gr1" {
  for_each = { for idx, m in local.metrics_gr1 : "${m.name}-${m.namespace}" => m }

  alarm_name                = "${each.value.name}-alarm-${each.key}"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "1"
  metric_name               = each.value.name
  namespace                 = each.value.namespace
  period                    = "300"
  statistic                 = each.value.statistic
  threshold                 = each.value.threshold
  alarm_description         = "Alarm when ${each.value.name} exceeds ${each.value.threshold}"
  dimensions                = { InstanceId = each.key }
  treat_missing_data        = "notBreaching"
}

# Création des alarmes CloudWatch
resource "aws_cloudwatch_metric_alarm" "instance_alarms_gr2" {
  for_each = { for idx, m in local.metrics_gr2 : "${m.name}-${m.namespace}" => m }

  alarm_name                = "${each.value.name}-alarm-${each.key}"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "1"
  metric_name               = each.value.name
  namespace                 = each.value.namespace
  period                    = "300"
  statistic                 = each.value.statistic
  threshold                 = each.value.threshold
  alarm_description         = "Alarm when ${each.value.name} exceeds ${each.value.threshold}"
  dimensions                = { InstanceId = each.key }
  treat_missing_data        = "notBreaching"
}

# Création des alarmes CloudWatch
resource "aws_cloudwatch_metric_alarm" "instance_alarms_gr3" {
  for_each = { for idx, m in local.metrics_gr3 : "${m.name}-${m.namespace}" => m }

  alarm_name                = "${each.value.name}-alarm-${each.key}"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "1"
  metric_name               = each.value.name
  namespace                 = each.value.namespace
  period                    = "300"
  statistic                 = each.value.statistic
  threshold                 = each.value.threshold
  alarm_description         = "Alarm when ${each.value.name} exceeds ${each.value.threshold}"
  dimensions                = { InstanceId = each.key }
  treat_missing_data        = "notBreaching"
}

# Création des alarmes CloudWatch
resource "aws_cloudwatch_metric_alarm" "instance_alarms_gr4" {
  for_each = { for idx, m in local.metrics_gr4 : "${m.name}-${m.namespace}" => m }

  alarm_name                = "${each.value.name}-alarm-${each.key}"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "1"
  metric_name               = each.value.name
  namespace                 = each.value.namespace
  period                    = "300"
  statistic                 = each.value.statistic
  threshold                 = each.value.threshold
  alarm_description         = "Alarm when ${each.value.name} exceeds ${each.value.threshold}"
  dimensions                = { InstanceId = each.key }
  treat_missing_data        = "notBreaching"
}

# Création des alarmes CloudWatch
resource "aws_cloudwatch_metric_alarm" "instance_alarms_gr5" {
  for_each = { for idx, m in local.metrics_gr5 : "${m.name}-${m.namespace}" => m }

  alarm_name                = "${each.value.name}-alarm-${each.key}"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "1"
  metric_name               = each.value.name
  namespace                 = each.value.namespace
  period                    = "300"
  statistic                 = each.value.statistic
  threshold                 = each.value.threshold
  alarm_description         = "Alarm when ${each.value.name} exceeds ${each.value.threshold}"
  dimensions                = { InstanceId = each.key }
  treat_missing_data        = "notBreaching"
}

# Configuration du Dashboard CloudWatch
resource "aws_cloudwatch_dashboard" "monitored_instances_dashboard_gr1" {
  dashboard_name = "Monitored-Instances-Dashboard-GR1"

  dashboard_body = jsonencode({
    widgets = [
      for m in local.metrics_gr1 : {
        type = "metric"
        x = 0
        y = 0
        width = 12
        height = 6
        properties = {
          metrics = [
            for id in local.monitored_gr1_instances : [m.namespace, m.name, "InstanceId", id]
          ]
          period = 300
          stat = m.statistic
          region = "eu-west-3"
          title = "${m.name} Metrics"
        }
      }
    ]
  })
}

# Configuration du Dashboard CloudWatch
resource "aws_cloudwatch_dashboard" "monitored_instances_dashboard_gr2" {
  dashboard_name = "Monitored-Instances-Dashboard-GR2"

  dashboard_body = jsonencode({
    widgets = [
      for m in local.metrics_gr2 : {
        type = "metric"
        x = 0
        y = 0
        width = 12
        height = 6
        properties = {
          metrics = [
            for id in local.monitored_gr2_instances : [m.namespace, m.name, "InstanceId", id]
          ]
          period = 300
          stat = m.statistic
          region = "eu-west-3"
          title = "${m.name} Metrics"
        }
      }
    ]
  })
}

# Configuration du Dashboard CloudWatch
resource "aws_cloudwatch_dashboard" "monitored_instances_dashboard_gr3" {
  dashboard_name = "Monitored-Instances-Dashboard-GR3"

  dashboard_body = jsonencode({
    widgets = [
      for m in local.metrics_gr3 : {
        type = "metric"
        x = 0
        y = 0
        width = 12
        height = 6
        properties = {
          metrics = [
            for id in local.monitored_gr3_instances : [m.namespace, m.name, "InstanceId", id]
          ]
          period = 300
          stat = m.statistic
          region = "eu-west-3"
          title = "${m.name} Metrics"
        }
      }
    ]
  })
}

# Configuration du Dashboard CloudWatch
resource "aws_cloudwatch_dashboard" "monitored_instances_dashboard_gr4" {
  dashboard_name = "Monitored-Instances-Dashboard-GR4"

  dashboard_body = jsonencode({
    widgets = [
      for m in local.metrics_gr4 : {
        type = "metric"
        x = 0
        y = 0
        width = 12
        height = 6
        properties = {
          metrics = [
            for id in local.monitored_gr4_instances : [m.namespace, m.name, "InstanceId", id]
          ]
          period = 300
          stat = m.statistic
          region = "eu-west-3"
          title = "${m.name} Metrics"
        }
      }
    ]
  })
}

# Configuration du Dashboard CloudWatch
resource "aws_cloudwatch_dashboard" "monitored_instances_dashboard_gr5" {
  dashboard_name = "Monitored-Instances-Dashboard-GR5"

  dashboard_body = jsonencode({
    widgets = [
      for m in local.metrics_gr5 : {
        type = "metric"
        x = 0
        y = 0
        width = 12
        height = 6
        properties = {
          metrics = [
            for id in local.monitored_gr5_instances : [m.namespace, m.name, "InstanceId", id]
          ]
          period = 300
          stat = m.statistic
          region = "eu-west-3"
          title = "${m.name} Metrics"
        }
      }
    ]
  })
}