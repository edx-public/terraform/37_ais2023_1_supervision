resource "aws_cloudwatch_log_group" "my_log_group" {
  name = "/ec2/37_AIS2024_1"
}

resource "aws_cloudwatch_dashboard" "example" {
  dashboard_name = "Dashboard-Exemple-37_AIS2024_1"
dashboard_body = jsonencode({
  "widgets": [
    {
      "type": "text",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 3,
      "properties": {
        "markdown": "## EC2 Instance Metrics"
      }
    },
    {
  "type": "metric",
  "x": 0,
  "y": 3,
  "width": 12,
  "height": 3,
  "properties": {
    "metrics": [
      ["AWS/EC2", "CPUUtilization", "InstanceId", aws_instance.teacher_instance_gr1.id],
      ["AWS/EC2", "CPUUtilization", "InstanceId", aws_instance.teacher_instance_gr2.id],
      ["AWS/EC2", "CPUUtilization", "InstanceId", aws_instance.teacher_instance_gr3.id],
      ["AWS/EC2", "CPUUtilization", "InstanceId", aws_instance.teacher_instance_gr4.id],
      ["AWS/EC2", "CPUUtilization", "InstanceId", aws_instance.teacher_instance_gr5.id]
    ],
    "period": 300,
    "stat": "Average",
    "region": "eu-west-3",
    "title": "CPU Utilization"
  }
},
{
  "type": "metric",
  "x": 0,
  "y": 12,
  "width": 12,
  "height": 3,
  "properties": {
    "metrics": [
      ["AWS/EC2", "DiskReadOps", "InstanceId", aws_instance.teacher_instance_gr1.id],
      ["AWS/EC2", "DiskReadOps", "InstanceId", aws_instance.teacher_instance_gr2.id],
      ["AWS/EC2", "DiskReadOps", "InstanceId", aws_instance.teacher_instance_gr3.id],
      ["AWS/EC2", "DiskReadOps", "InstanceId", aws_instance.teacher_instance_gr4.id],
      ["AWS/EC2", "DiskReadOps", "InstanceId", aws_instance.teacher_instance_gr5.id]
    ],
    "period": 300,
    "stat": "Average",
    "region": "eu-west-3",
    "title": "Disk Read Operations"
  }
},
{
  "type": "metric",
  "x": 0,
  "y": 15,
  "width": 12,
  "height": 3,
  "properties": {
    "metrics": [
      ["AWS/EC2", "DiskWriteOps", "InstanceId", aws_instance.teacher_instance_gr1.id],
      ["AWS/EC2", "DiskWriteOps", "InstanceId", aws_instance.teacher_instance_gr2.id],
      ["AWS/EC2", "DiskWriteOps", "InstanceId", aws_instance.teacher_instance_gr3.id],
      ["AWS/EC2", "DiskWriteOps", "InstanceId", aws_instance.teacher_instance_gr4.id],
      ["AWS/EC2", "DiskWriteOps", "InstanceId", aws_instance.teacher_instance_gr5.id]
    ],
    "period": 300,
    "stat": "Average",
    "region": "eu-west-3",
    "title": "Disk Write Operations"
  }
},
{
  "type": "metric",
  "x": 0,
  "y": 6,
  "width": 12,
  "height": 3,
  "properties": {
    "metrics": [
      ["AWS/EC2", "NetworkIn", "InstanceId", aws_instance.teacher_instance_gr1.id],
      ["AWS/EC2", "NetworkIn", "InstanceId", aws_instance.teacher_instance_gr2.id],
      ["AWS/EC2", "NetworkIn", "InstanceId", aws_instance.teacher_instance_gr3.id],
      ["AWS/EC2", "NetworkIn", "InstanceId", aws_instance.teacher_instance_gr4.id],
      ["AWS/EC2", "NetworkIn", "InstanceId", aws_instance.teacher_instance_gr5.id]
    ],
    "period": 300,
    "stat": "Average",
    "region": "eu-west-3",
    "title": "Network In"
  }
},
{
  "type": "metric",
  "x": 0,
  "y": 9,
  "width": 12,
  "height": 3,
  "properties": {
    "metrics": [
      ["AWS/EC2", "NetworkOut", "InstanceId", aws_instance.teacher_instance_gr1.id],
      ["AWS/EC2", "NetworkOut", "InstanceId", aws_instance.teacher_instance_gr2.id],
      ["AWS/EC2", "NetworkOut", "InstanceId", aws_instance.teacher_instance_gr3.id],
      ["AWS/EC2", "NetworkOut", "InstanceId", aws_instance.teacher_instance_gr4.id],
      ["AWS/EC2", "NetworkOut", "InstanceId", aws_instance.teacher_instance_gr5.id]
    ],
    "period": 300,
    "stat": "Average",
    "region": "eu-west-3",
    "title": "Network Out"
  }
}
  ]
})

}

resource "aws_iam_role" "ec2_cloudwatch_role" {
  name = "ec2_cloudwatch_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Effect = "Allow",
        Sid    = ""
      },
    ]
  })
}

resource "aws_iam_policy" "cloudwatch_policy" {
  name        = "cloudwatch_policy"
  description = "A policy that allows publishing logs and metrics to CloudWatch"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "cloudwatch:PutMetricData",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:CreateLogGroup",
          "cloudwatch:GetDashboard",
          "ec2:DescribeTags",
          "cloudwatch:PutDashboard"
        ],
        Effect   = "Allow",
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "cloudwatch_policy_attachment" {
  role       = aws_iam_role.ec2_cloudwatch_role.name
  policy_arn = aws_iam_policy.cloudwatch_policy.arn
}

resource "aws_iam_instance_profile" "ec2_cloudwatch_profile" {
  name = "ec2_cloudwatch_profile"
  role = aws_iam_role.ec2_cloudwatch_role.name
}

resource "aws_instance" "teacher_instance_gr1" {
  ami           = "ami-05073582a4b03d785"
  subnet_id     = aws_subnet.vlan64_gr1.id
  instance_type = "t3.micro"
  key_name      = "KeyPairAISTeacher"
   depends_on = [
    aws_ec2_client_vpn_network_association.subnet_assoc_gr1,
    aws_ec2_client_vpn_authorization_rule.vpn_access_gr1
  ]

  iam_instance_profile = aws_iam_instance_profile.ec2_cloudwatch_profile.name

  user_data = <<-EOF
            #!/bin/bash
                {
                    echo $(date) | tee -a /root/.begin_first-install-script
                    curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/cloudwatch_integration.sh?ref_type=heads | sh
                    echo $(date) | tee -a /root/.end_first-install-script
                } |& tee /var/log/first-installation-script.log
            EOF
  tags = {
    Name = "CloudWatchInstance_gr1"
    Backup = "true"
  }
}

resource "aws_instance" "teacher_instance_gr2" {
  ami           = "ami-05073582a4b03d785"
  subnet_id     = aws_subnet.vlan64_gr2.id
  instance_type = "t3.micro"
  key_name      = "KeyPairAISTeacher"
   depends_on = [
    aws_ec2_client_vpn_network_association.subnet_assoc_gr2,
    aws_ec2_client_vpn_authorization_rule.vpn_access_gr2
  ]

  iam_instance_profile = aws_iam_instance_profile.ec2_cloudwatch_profile.name

  user_data = <<-EOF
            #!/bin/bash
                {
                    echo $(date) | tee -a /root/.begin_first-install-script
                    curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/cloudwatch_integration.sh?ref_type=heads | sh
                    echo $(date) | tee -a /root/.end_first-install-script
                } |& tee /var/log/first-installation-script.log
            EOF
  tags = {
    Name = "CloudWatchInstance_gr2"
    Backup = "true"
  }
}

resource "aws_instance" "teacher_instance_gr3" {
  ami           = "ami-05073582a4b03d785"
  subnet_id     = aws_subnet.vlan64_gr3.id
  instance_type = "t3.micro"
  key_name      = "KeyPairAISTeacher"
   depends_on = [
    aws_ec2_client_vpn_network_association.subnet_assoc_gr3,
    aws_ec2_client_vpn_authorization_rule.vpn_access_gr3
  ]
  iam_instance_profile = aws_iam_instance_profile.ec2_cloudwatch_profile.name

  user_data = <<-EOF
            #!/bin/bash
                {
                    echo $(date) | tee -a /root/.begin_first-install-script
                    curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/cloudwatch_integration.sh?ref_type=heads | sh
                    echo $(date) | tee -a /root/.end_first-install-script
                } |& tee /var/log/first-installation-script.log
            EOF
  tags = {
    Name = "CloudWatchInstance_gr3"
    Backup = "true"
  }
}

resource "aws_instance" "teacher_instance_gr4" {
  ami           = "ami-05073582a4b03d785"
  subnet_id     = aws_subnet.vlan64_gr4.id
  instance_type = "t3.micro"
  key_name      = "KeyPairAISTeacher"
   depends_on = [
    aws_ec2_client_vpn_network_association.subnet_assoc_gr4,
    aws_ec2_client_vpn_authorization_rule.vpn_access_gr4
  ]
  iam_instance_profile = aws_iam_instance_profile.ec2_cloudwatch_profile.name

  user_data = <<-EOF
            #!/bin/bash
                {
                    echo $(date) | tee -a /root/.begin_first-install-script
                    curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/cloudwatch_integration.sh?ref_type=heads | sh
                    echo $(date) | tee -a /root/.end_first-install-script
                } |& tee /var/log/first-installation-script.log
            EOF
  tags = {
    Name = "CloudWatchInstance_gr4"
    Backup = "true"
  }
}

resource "aws_instance" "teacher_instance_gr5" {
  ami           = "ami-05073582a4b03d785"
  subnet_id     = aws_subnet.vlan64_gr5.id
  instance_type = "t3.micro"
  key_name      = "KeyPairAISTeacher"
   depends_on = [
    aws_ec2_client_vpn_network_association.subnet_assoc_gr5,
    aws_ec2_client_vpn_authorization_rule.vpn_access_gr5
  ]
  iam_instance_profile = aws_iam_instance_profile.ec2_cloudwatch_profile.name

  user_data = <<-EOF
            #!/bin/bash
                {
                    echo $(date) | tee -a /root/.begin_first-install-script
                    curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/cloudwatch_integration.sh?ref_type=heads | sh
                    echo $(date) | tee -a /root/.end_first-install-script
                } |& tee /var/log/first-installation-script.log
            EOF
  tags = {
    Name = "CloudWatchInstance_gr5"
    Backup = "true"
  }
}