#!/bin/bash
for source in ./instance-?-?_gr1.tf; do
    for group in {2..5}; do
        target="./$(echo "$source" | sed "s/_gr1.tf/_gr${group}.tf/")" # Construit le nom du fichier cible
        cp "${source}" "${target}"
        
        # Détecter si l'on utilise GNU sed ou BSD sed (macOS)
        if sed --version >/dev/null 2>&1; then
            # GNU sed
            sed -i "s/101\./10${group}\./g" "${target}"
            sed -i "s/gr1/gr${group}/g" "${target}"
            sed -i "s/Gr1/Gr${group}/g" "${target}"
        else
            # BSD sed (macOS)
            sed -i '' "s/101\./10${group}\./g" "${target}"
            sed -i '' "s/gr1/gr${group}/g" "${target}"
            sed -i '' "s/Gr1/Gr${group}/g" "${target}"
        fi
    done
done
echo "Tous les fichiers de configuration Instances pour les groupes 2 à 5 ont été générés."
