def convert_ssh_config_to_sntop(input_path, output_path):
    with open(input_path, 'r') as file:
        ssh_config_lines = file.readlines()

    sntop_config_lines = []
    for i in range(len(ssh_config_lines)):
        line = ssh_config_lines[i].strip()
        if line.startswith('Host ') and not line.startswith('Host *'):
            host_aliases = line.split()[1:]
            host_name = host_aliases[-1]
            j = i + 1
            hostname = None
            while j < len(ssh_config_lines) and not hostname:
                next_line = ssh_config_lines[j].strip()
                if next_line.startswith('Hostname'):
                    hostname = next_line.split()[1]
                j += 1
            if hostname:
                sntop_config_lines.append(f"{host_name}\n{hostname}\n{host_name}")
    
    with open(output_path, 'w') as file:
        for line in sntop_config_lines:
            file.write(f"{line}\n\n")

# Liste des fichiers de configuration ssh et des fichiers de sortie sntop
files_to_process = [
    ('./ssh_config/config_gr1', './sntop/sntop_gr1.txt'),
    ('./ssh_config/config_gr2', './sntop/sntop_gr2.txt'),
    ('./ssh_config/config_gr3', './sntop/sntop_gr3.txt'),
    ('./ssh_config/config_gr4', './sntop/sntop_gr4.txt'),
    ('./ssh_config/config_gr5', './sntop/sntop_gr5.txt'),
]

# Boucle pour traiter chaque paire de fichiers
for input_path, output_path in files_to_process:
    convert_ssh_config_to_sntop(input_path, output_path)
    print(f"Le fichier de configuration sntop a été créé à {output_path}")

# Génération du fichier sntop_all.txt par la concaténation de tous les fichiers sntop
all_output_path = './sntop/sntop_all.txt'
with open(all_output_path, 'w') as all_file:
    for _, output_path in files_to_process:
        with open(output_path, 'r') as file:
            # Écrire le contenu de chaque fichier sntop dans sntop_all.txt
            all_file.write(file.read() + "\n\n")
print(f"Le fichier de configuration sntop_all a été créé à {all_output_path}")