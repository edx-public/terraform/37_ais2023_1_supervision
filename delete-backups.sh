#!/bin/bash

# Listez tous les coffres de sauvegarde
vaults=$(aws backup list-backup-vaults --query "BackupVaultList[].BackupVaultName" --output text)

# Pour chaque coffre, listez tous les points de récupération et supprimez-les
for vault in $vaults; do
  recoveryPoints=$(aws backup list-recovery-points-by-backup-vault --backup-vault-name "$vault" --query "RecoveryPoints[].RecoveryPointArn" --output text)
  for recoveryPoint in $recoveryPoints; do
   aws backup delete-recovery-point --backup-vault-name "$vault" --recovery-point-arn "$recoveryPoint"
   echo "Deleted recovery point $recoveryPoint in vault $vault"
  done
done
