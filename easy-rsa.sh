#!/bin/bash

# Définition du domaine pour lequel les certificats doivent être supprimés
DOMAIN="aws.cefim.intra"

# Liste et supprime tous les certificats ACM pour le domaine spécifié
aws acm list-certificates --query "CertificateSummaryList[?contains(DomainName, '$DOMAIN')].[CertificateArn]" --output text | tr '\t' '\n' | while read arn; do
    echo "Suppression du certificat : $arn"
    aws acm delete-certificate --certificate-arn "$arn"
done

echo "Tous les certificats pour le domaine '$DOMAIN' ont été supprimés."

export EASYRSA_BATCH="1"  # Active le mode batch pour éviter les interactions manuelles

# Chemin vers le répertoire Easy-RSA
EASY_RSA_PATH="../easy-rsa/easyrsa3"
export EASYRSA="$EASY_RSA_PATH"
export EASYRSA_PKI="./pki"

rm -fr $EASYRSA_PKI

# Initialisation de l'espace de travail Easy-RSA et création du CA
$EASYRSA/easyrsa init-pki
echo "aws.cefim.intra" | $EASYRSA/easyrsa build-ca nopass

# Boucle à travers les groupes et les utilisateurs
for group in {1..5}; do
    # Générer et signer le certificat serveur pour chaque groupe
    server_name="server-gr${group}.aws.cefim.intra"
    $EASYRSA/easyrsa gen-req $server_name nopass
    echo "yes" | $EASYRSA/easyrsa sign-req server $server_name

    for user in {1..5}; do
        # Générer et signer le certificat client pour chaque utilisateur dans le groupe
        client_name="client${user}-gr${group}.aws.cefim.intra"
        $EASYRSA/easyrsa gen-req $client_name nopass
        echo "yes" | $EASYRSA/easyrsa sign-req client $client_name
    done
    client_name_teacher="teacher-gr${group}.aws.cefim.intra"
    $EASYRSA/easyrsa gen-req $client_name_teacher nopass
    echo "yes" | $EASYRSA/easyrsa sign-req client $client_name_teacher
done

# Générer le fichier de configuration CRL
$EASYRSA/easyrsa gen-crl 2> /dev/null

echo "Les certificats pour les serveurs et les clients ont été générés avec succès."

# Fichier de sortie pour les variables Terraform
TFVARS_FILE="certificates.auto.tfvars"
echo "" > $TFVARS_FILE

ca_cert_arn=$(aws acm import-certificate \
    --certificate fileb://${EASYRSA_PKI}/ca.crt \
    --private-key fileb://${EASYRSA_PKI}/private/ca.key \
    --query CertificateArn --output text)
echo "ca_certificate_arn = \"$ca_cert_arn\"" >> $TFVARS_FILE

# Importation des certificats et génération du fichier tfvars
for group in {1..5}; do
    # Serveur
    server_name="server-gr${group}.aws.cefim.intra"
    server_cert_arn=$(aws acm import-certificate \
        --certificate fileb://${EASYRSA_PKI}/issued/${server_name}.crt \
        --private-key fileb://${EASYRSA_PKI}/private/${server_name}.key \
        --certificate-chain fileb://${EASYRSA_PKI}/ca.crt \
        --query CertificateArn --output text)
    echo "server_gr${group}_certificate_arn = \"$server_cert_arn\"" >> $TFVARS_FILE

    # Clients
    for client in {1..5}; do
        client_name="client${client}-gr${group}.aws.cefim.intra"
        client_cert_arn=$(aws acm import-certificate \
            --certificate fileb://${EASYRSA_PKI}/issued/${client_name}.crt \
            --private-key fileb://${EASYRSA_PKI}/private/${client_name}.key \
            --certificate-chain fileb://${EASYRSA_PKI}/ca.crt \
            --query CertificateArn --output text)
        echo "client${client}_gr${group}_certificate_arn = \"$client_cert_arn\"" >> $TFVARS_FILE
    done
done

echo "Les certificats ont été importés et le fichier $TFVARS_FILE a été généré."
