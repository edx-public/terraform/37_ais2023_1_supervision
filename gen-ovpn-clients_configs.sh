#!/bin/bash

DOMAIN="aws.cefim.intra"
# Récupération du chemin absolu du répertoire courant
CURRENT_DIR=$(pwd)
# Emplacement de base pour les certificats et clés
PKI_DIR="${CURRENT_DIR}/pki"
# Emplacement pour stocker les archives finales
OUTPUT_DIR="${CURRENT_DIR}/clients_ovpn_configs"
install -d "${OUTPUT_DIR}"
# Emplacement du certificat de l'autorité de certification
CA_CERT="${PKI_DIR}/ca.crt"

for group in {1..5}; do
    for client in {1..5}; do
        CLIENT_NAME="client${client}-gr${group}"
        TEMP_DIR=$(mktemp -d)
        # Copie de la configuration OpenVPN, des certificats, et de la clé dans le répertoire temporaire
        cp "${PKI_DIR}/issued/${CLIENT_NAME}.${DOMAIN}.crt" "${TEMP_DIR}/client.crt"
        cp "${PKI_DIR}/private/${CLIENT_NAME}.${DOMAIN}.key" "${TEMP_DIR}/client.key"
        cp "${CA_CERT}" "${TEMP_DIR}/ca.crt"
        # Supposons que le fichier de configuration .ovpn est nommé de manière similaire et stocké dans un emplacement spécifique
        cp "./ovpn_configs/${CLIENT_NAME}.ovpn" "${TEMP_DIR}/config.ovpn"
        # Modification de la configuration OpenVPN pour utiliser les chemins relatifs
        # Moins pratique que sed -i - mais fonctionne aussi sous MacOS
        awk '/^ca/{$0="ca ./ca.crt"}1' "${TEMP_DIR}/config.ovpn" > "${TEMP_DIR}/config_temp.ovpn" && mv "${TEMP_DIR}/config_temp.ovpn" "${TEMP_DIR}/config.ovpn"
        awk '/^cert/{$0="cert ./client.crt"}1' "${TEMP_DIR}/config.ovpn" > "${TEMP_DIR}/config_temp.ovpn" && mv "${TEMP_DIR}/config_temp.ovpn" "${TEMP_DIR}/config.ovpn"
        awk '/^key/{$0="key ./client.key"}1' "${TEMP_DIR}/config.ovpn" > "${TEMP_DIR}/config_temp.ovpn" && mv "${TEMP_DIR}/config_temp.ovpn" "${TEMP_DIR}/config.ovpn"
        # Création de l'archive ZIP
        rm -f "${OUTPUT_DIR}/${CLIENT_NAME}.zip"
        (cd "${TEMP_DIR}" && zip -r "${OUTPUT_DIR}/${CLIENT_NAME}.zip" ./*)
        # Suppression du répertoire temporaire
        rm -rf "${TEMP_DIR}"
    done
done

echo "Les configurations des clients ont été préparées."
