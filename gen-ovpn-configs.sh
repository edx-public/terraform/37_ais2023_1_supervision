#!/bin/bash

# Emplacement où stocker les fichiers de configuration .ovpn
OUTPUT_DIR="./ovpn_configs"
mkdir -p $OUTPUT_DIR
CERTS_DIR="../pki/issued"
KEYS_DIR="../pki/private"
CA_DIR="../pki"

# Récupération du FQDN ou de l'adresse IP du Client VPN Endpoint
# ENDPOINT_ADDRESS=$(aws ec2 describe-client-vpn-endpoints --query "ClientVpnEndpoints[0].DnsName" --output text)

# Boucle pour générer les fichiers .ovpn pour les clients 1 à 5 dans les groupes 1 à 5
for group in {1..5}; do
ENDPOINT_ADDRESS=$(aws ec2 describe-client-vpn-endpoints --query "ClientVpnEndpoints[?ClientCidrBlock=='10.${group}.0.0/16'].DnsName" --output text)
if [ -z "$ENDPOINT_ADDRESS" ]; then
    echo "Adresse de l'endpoint VPN non trouvée. Vérifiez votre VPN_ENDPOINT_ID et vos permissions AWS."
    exit 1
fi
MODIFIED_ADDRESS=$(echo $ENDPOINT_ADDRESS | sed 's/\*\./ovpn\./')
    for client in {1..5}; do
        cat > $OUTPUT_DIR/client${client}-gr${group}.ovpn << EOF
client
dev tun
proto udp
remote $MODIFIED_ADDRESS 443
resolv-retry infinite
keepalive 10 120
nobind
persist-key
persist-tun
remote-cert-tls server

# Chemins vers les fichiers de certificat et de clé
ca ${CA_DIR}/ca.crt
cert ${CERTS_DIR}/client${client}-gr${group}.aws.cefim.intra.crt
key ${KEYS_DIR}/client${client}-gr${group}.aws.cefim.intra.key

verb 3
EOF

        echo "Fichier généré: $OUTPUT_DIR/client${client}-gr${group}.ovpn"
    done

    cat > $OUTPUT_DIR/teacher-gr${group}.ovpn << EOF
client
dev tun
proto udp
remote $MODIFIED_ADDRESS 443
resolv-retry infinite
keepalive 10 120
nobind
persist-key
persist-tun
remote-cert-tls server

# Chemins vers les fichiers de certificat et de clé
ca ${CA_DIR}/ca.crt
cert ${CERTS_DIR}/teacher-gr${group}.aws.cefim.intra.crt
key ${KEYS_DIR}/teacher-gr${group}.aws.cefim.intra.key

verb 3
EOF
    echo "Fichier généré: $OUTPUT_DIR/teacher-gr${group}.ovpn"
done

echo "Tous les fichiers de configuration .ovpn ont été générés."
