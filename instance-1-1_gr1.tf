resource "aws_network_interface" "vip_instance1-1_vlan48_gr1" {
  subnet_id       = aws_subnet.vlan48_gr1.id
  private_ips     = ["192.168.101.55"]
  security_groups = [aws_security_group.sg_vlan48_gr1.id]
}

resource "aws_instance" "instance1-1_gr1" {
  # terraform taint aws_instance.instance1-1_gr1
  # terraform apply -auto-approve -var-file="main.private.tfvars"
  ami           = var.ami_instance1-1
  instance_type = "t3.micro"
  key_name      = "KeyPairAISGr1"
  depends_on = [
    aws_ec2_client_vpn_network_association.subnet_assoc_gr1,
    aws_ec2_client_vpn_authorization_rule.vpn_access_gr1
  ]

  network_interface {
    network_interface_id = aws_network_interface.vip_instance1-1_vlan48_gr1.id
    device_index         = 0
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 64 # Taille du volume en GB
  }
user_data = <<-EOF
              #!/bin/bash
              {
                echo $(date) | tee -a /root/.begin_first-install-script
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/common.sh?ref_type=heads | sh
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/docker.sh?ref_type=heads | sh
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/instance1-1.sh?ref_type=heads | sh
                echo $(date) | tee -a /root/.end_first-install-script
              } |& tee /var/log/first-installation-script.log
              EOF

  tags = {
    Name = "instance1-1_gr1"
    Backup = "true"
  }
}

## Association d'IP Publique à une Instance
# resource "aws_eip_association" "eip_assoc_gr1" {
#   allocation_id = "eipalloc-068e5c8cf8320c172" # Remplacez par votre AllocationId réel
#   instance_id   = aws_instance.instance1_gr1.id
# }