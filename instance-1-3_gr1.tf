resource "aws_network_interface" "vip_instance1-3_vlan48_gr1" {
  subnet_id       = aws_subnet.vlan48_gr1.id
  private_ips     = ["192.168.101.60"]
  security_groups = [aws_security_group.sg_vlan48_gr1.id]
}

resource "aws_instance" "instance1-3_gr1" {
  ami           = var.ami_instance1-3
  instance_type = "t3.large"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance1-2_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance1-3_vlan48_gr1.id
    device_index         = 0
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 128 # Taille du volume en GB
  }
user_data = <<-EOF
              #!/bin/bash
              {
                echo $(date) | tee -a /root/.begin_first-install-script
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/common.sh?ref_type=heads | sh
                sed -i 's/#Port 22/Port 64295/' /etc/ssh/sshd_config
                systemctl restart ssh
                # git clone https://github.com/dtag-dev-sec/tpotce
                # cd tpotce/ && ./install.sh
                # echo $(date) | tee -a /root/.end_first-install-script
                # https://medium.com/@mkmety/deploying-t-pot-the-all-in-one-honeypot-platform-on-aws-ec2-33f019c645fb
                # https://192.168.10?.60:64297
              } |& tee /var/log/first-installation-script.log
              EOF
  tags = {
    Name = "instance1-3_gr1"
    Backup = "true"
  }
}