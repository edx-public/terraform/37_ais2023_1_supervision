resource "aws_network_interface" "vip_instance1-4_vlan48_gr1" {
  subnet_id       = aws_subnet.vlan48_gr1.id
  private_ips     = ["192.168.101.57"]
  security_groups = [aws_security_group.sg_vlan48_gr1.id]
}

resource "aws_instance" "instance1-4_gr1" {
  ami           = var.ami_instance1-4
  instance_type = "t3.micro"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance1-3_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance1-4_vlan48_gr1.id
    device_index         = 0
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 64 # Taille du volume en GB
  }
  tags = {
    Name = "instance1-4_gr1"
    Backup = "true"
  }
}
