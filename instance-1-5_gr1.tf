resource "aws_network_interface" "vip_instance1-5_vlan48_gr1" {
  subnet_id       = aws_subnet.vlan48_gr1.id
  private_ips     = ["192.168.101.52"]
  security_groups = [aws_security_group.sg_vlan48_gr1.id]
}

resource "aws_instance" "instance1-5_gr1" {
  ami           = var.ami_instance1-5
  instance_type = "c5.xlarge"
  key_name      = "KeyPairAISTeacher"
  depends_on = [aws_instance.instance1-4_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance1-5_vlan48_gr1.id
    device_index         = 0
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 256 # Taille du volume en GB
  }
  user_data = <<-EOF
              #!/bin/bash
              {
                echo $(date) | tee -a /root/.begin_first-install-script
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/common.sh?ref_type=heads | sh
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/docker.sh?ref_type=heads | sh
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/use-pihole.sh?ref_type=heads | sh
                echo $(date) | tee -a /root/.end_first-install-script
              } |& tee /var/log/first-installation-script.log
              EOF
  tags = {
    Name = "instance1-5_gr1"
    Backup = "true"
  }
}
