resource "aws_network_interface" "vip_instance2-1_vlan64_gr1" {
  subnet_id       = aws_subnet.vlan64_gr1.id
  private_ips     = ["192.168.101.75"]
  security_groups = [aws_security_group.sg_vlan64_gr1.id]
}

resource "aws_instance" "instance2-1_gr1" {
  ami           = var.ami_instance2-1
  instance_type = "t3.micro"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance1-4_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance2-1_vlan64_gr1.id
    device_index         = 0
  }
root_block_device {
    volume_type = "gp3"
    volume_size = 64 # Taille du volume en GB
  }
  tags = {
    Name = "instance2-1_gr1"
    Backup = "true"
  }
}
