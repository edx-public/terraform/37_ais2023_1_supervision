# https://docs.fedoraproject.org/en-US/fedora-coreos/provisioning-aws/

resource "aws_network_interface" "vip_instance2-2_vlan64_gr1" {
  subnet_id       = aws_subnet.vlan64_gr1.id
  private_ips     = ["192.168.101.78"]
  security_groups = [aws_security_group.sg_vlan64_gr1.id]
}

resource "aws_instance" "instance2-2_gr1" {
  ami           = var.ami_instance2-2
  instance_type = "t3.micro"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance2-1_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance2-2_vlan64_gr1.id
    device_index         = 0
  }
root_block_device {
    volume_type = "gp3"
    volume_size = 64 # Taille du volume en GB
  }
  # user_data = <<-EOF
  #   podman run -d --name=netdata \
  #     -p 19999:19999 \
  #     -v /proc:/host/proc:ro \
  #     -v /sys:/host/sys:ro \
  #     -v /var/run/docker.sock:/var/run/docker.sock:ro \
  #     --cap-add SYS_PTRACE \
  #     --security-opt label=disable \
  #     quay.io/netdata/netdata:latest
  #   podman generate systemd --name netdata --files
  #   cp container-netdata.service /etc/systemd/system
  #   systemctl daemon-reload
  #   systemctl enable container-netdata.service
  #   systemctl start container-netdata.service
  #   EOF

  tags = {
    Name = "instance2-2_gr1"
    Backup = "true"
  }
}