resource "aws_network_interface" "vip_instance2-3_vlan64_gr1" {
  subnet_id       = aws_subnet.vlan64_gr1.id
  private_ips     = ["192.168.101.101"]
  security_groups = [aws_security_group.sg_vlan64_gr1.id]
}

resource "aws_instance" "instance2-3_gr1" {
  ami           = var.ami_instance_etudiant
  instance_type = "t3.xlarge"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance2-2_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance2-3_vlan64_gr1.id
    device_index         = 0
  }
  user_data = <<-EOF
              #!/bin/bash
              {
                echo $(date) | tee -a /root/.begin_first-install-script
                echo "Ajout des configurations pour désactiver l'IPv6..."
                echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> /etc/sysctl.conf
                echo 'net.ipv6.conf.default.disable_ipv6 = 1' >> /etc/sysctl.conf
                echo 'net.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.conf

                # Application des changements
                echo "Application des changements..."
                sysctl -p
                echo $(date) | tee -a /root/.end_first-install-script
              } |& tee /var/log/first-installation-script.log
              EOF
root_block_device {
    volume_type = "gp3"
    volume_size = 64 # Taille du volume en GB
  }
  tags = {
    Name = "instance2-3_gr1"
    Backup = "true"
    Monitored_gr1 = "true"
  }
}
