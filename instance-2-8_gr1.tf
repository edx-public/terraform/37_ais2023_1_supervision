resource "aws_network_interface" "vip_instance2-8_vlan64_gr1" {
  subnet_id       = aws_subnet.vlan64_gr1.id
  private_ips     = ["192.168.101.81"]
  security_groups = [aws_security_group.sg_vlan64_gr1.id]
}

resource "aws_instance" "instance2-8_gr1" {
  ami           = var.ami_instance2-8
  instance_type = "t4g.micro"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance2-7_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance2-8_vlan64_gr1.id
    device_index         = 0
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 64 # Taille du volume en GB
  }
  user_data = <<-EOF
              #!/bin/bash
              {
                echo $(date) | tee -a /root/.begin_first-install-script
                echo "Ajout des configurations pour désactiver l'IPv6..."
                echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> /etc/sysctl.conf
                echo 'net.ipv6.conf.default.disable_ipv6 = 1' >> /etc/sysctl.conf
                echo 'net.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.conf

                # Application des changements
                echo "Application des changements..."
                sysctl -p
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/instance2-8.sh?ref_type=heads | sh
                echo $(date) | tee -a /root/.end_first-install-script
              } |& tee /var/log/first-installation-script.log
              EOF
  tags = {
    Name = "instance2-8_gr1"
    Backup = "true"
  }
}
