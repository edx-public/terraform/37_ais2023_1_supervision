resource "aws_network_interface" "vip_instance3-1_vlan128_gr1" {
  subnet_id       = aws_subnet.vlan128_gr1.id
  private_ips     = ["192.168.101.135"]
  security_groups = [aws_security_group.sg_vlan128_gr1.id]
}

resource "aws_instance" "instance3-1_gr1" {
  # docker run --rm colinianking/stress-ng:latest --cpu 4 --io 2 --vm 2 --vm-bytes 1G --hdd 2 --timeout 120s
  ami           = var.ami_instance3-1
  instance_type = "t3.micro"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance2-8_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance3-1_vlan128_gr1.id
    device_index         = 0
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 64 # Taille du volume en GB
  }
user_data = <<-EOF
              #!/bin/bash
              {
                echo $(date) | tee -a /root/.begin_first-install-script
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/common.sh?ref_type=heads | sh
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/docker.sh?ref_type=heads | sh
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/instance3-1.sh?ref_type=heads | sh
                curl -sL https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/user_data/use-pihole.sh?ref_type=heads | sh
                echo $(date) | tee -a /root/.end_first-install-script
              } |& tee /var/log/first-installation-script.log
              EOF
  tags = {
    Name = "instance3-1_gr1"
    Backup = "true"
  }
}