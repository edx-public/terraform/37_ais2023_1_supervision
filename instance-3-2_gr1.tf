resource "aws_network_interface" "vip_instance3-2_vlan128_gr1" {
  subnet_id       = aws_subnet.vlan128_gr1.id
  private_ips     = ["192.168.101.139"]
  security_groups = [aws_security_group.sg_vlan128_gr1.id]
}

resource "aws_instance" "instance3-2_gr1" {
  ami           = var.ami_instance3-2
  instance_type = "t3.medium"
  key_name      = "KeyPairAISGr1"
  depends_on = [aws_instance.instance3-1_gr1]
  network_interface {
    network_interface_id = aws_network_interface.vip_instance3-2_vlan128_gr1.id
    device_index         = 0
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 128 # Taille du volume en GB
  }
  tags = {
    Name = "instance3-2_gr1"
    Backup = "true"
  }
}
