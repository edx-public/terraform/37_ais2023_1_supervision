resource "aws_network_interface" "vip_instance3-4_vlan128_gr1" {
    subnet_id       = aws_subnet.vlan128_gr1.id
    private_ips     = ["192.168.101.142"]
    security_groups = [aws_security_group.sg_vlan128_gr1.id]
}

# https://stackoverflow.com/questions/50604127/how-to-execute-powershell-command-through-terraform
# aws ec2 describe-instance-status --query "InstanceStatuses[*].[InstanceId,InstanceState.Name,SystemStatus.Status,InstanceStatus.Status]" --output table
# xfreerdp /u:Administrator /v:192.168.10?.142
# xfreerdp  /cert-ignore /u:sysadmin /p:Windows123$ /v:192.168.101.142:3389
resource "aws_instance" "instance3-4_gr1" {
    ami           = var.ami_instance3-4
    instance_type = "t3.large"
    key_name      = "KeyPairAISGr1"
    depends_on = [aws_instance.instance3-3_gr1]
    network_interface {
        network_interface_id = aws_network_interface.vip_instance3-4_vlan128_gr1.id
        device_index         = 0
    }
    root_block_device {
        volume_type = "gp3"
        volume_size = 128 # Taille du volume en GB
    }
    # user_data = base64encode(file("${"./user_data/base64_create_sysadmin.ps1.txt"}"))
    user_data = <<-EOF
                <powershell>
                Start-Transcript -Path C:\first-install-log.txt -Append
                $adminPassword = "Windows123$"
                # $userName = "Administrator"
                # net user $userName $adminPassword
                $localUserName = "Sysadmin"
                $securePassword = ConvertTo-SecureString $adminPassword -AsPlainText -Force
                New-LocalUser -Name $localUserName -Password $securePassword -FullName "System Administrator" -Description "Compte Sysadmin"
                Add-LocalGroupMember -Group "Administrators" -Member $localUserName
                Install-WindowsFeature -name Web-Server -IncludeManagementTools
                Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
                # Install-ADDSForest -DomainName "aws.cefim.eu" -InstallDNS -Force
                Import-Module ADDSDeployment
                $domainName = "aws.cefim.intra" # Nom de domaine DNS à créer
                $safeModeAdministratorPassword = ConvertTo-SecureString "Windows123!" -AsPlainText -Force # Mot de passe pour le mode sans échec AD DS
                $domainNetBIOSName = "AWSCEFIM" # Nom NetBIOS du domaine
                Install-ADDSForest `
                    -DomainName $domainName `
                    -SafeModeAdministratorPassword $safeModeAdministratorPassword `
                    -DomainNetbiosName $domainNetBIOSName `
                    -InstallDNS `
                    -Force
                Stop-Transcript
                </powershell>
                EOF
    tags = {
    Name = "instance3-4_gr1"
    Backup = "true"
    }
}