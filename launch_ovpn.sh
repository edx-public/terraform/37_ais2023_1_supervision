#! /bin/bash
# https://wiki.debian.org/openvpn%20for%20server%20and%20client

arch_name="$(uname -m)"
if [ "${arch_name}" = "arm64" ] && [[ "$OSTYPE" == "darwin"* ]]; then
    ARCHITECTURE="M1"
    OPENVPN_BIN="/opt/homebrew/opt/openvpn/sbin/openvpn"
else
    ARCHITECTURE="X86"
    OPENVPN_BIN="sudo /usr/sbin/openvpn"
fi

# Vérifie si le premier argument est 'kill'
if [ "$1" = 'kill' ]; then
    echo "Arrêt de tous les processus OpenVPN..."
    sudo /usr/bin/killall openvpn 2> /dev/null
    exit 0
fi

if [ "$1" = 'config' ] ; then
        echo "Cmnd_Alias OPENVPN_COMMANDS = $OPENVPN_BIN, /usr/bin/killall openvpn" | sudo tee /etc/sudoers.d/openvpn > /dev/null
        echo "ALL ALL=(ALL) NOPASSWD: OPENVPN_COMMANDS" | sudo tee -a /etc/sudoers.d/openvpn > /dev/null
        exit 0
fi

# Lance les configurations OpenVPN en fonction de l'argument fourni
if [ "$1" = 'all' ]; then
    echo "Démarrage de toutes les configurations OpenVPN..."
    sudo $OPENVPN_BIN --script-security 2 --config ./teacher-gr1.ovpn 2>&1 >/dev/null &
    sudo $OPENVPN_BIN --script-security 2 --config ./teacher-gr2.ovpn 2>&1 >/dev/null &
    sudo $OPENVPN_BIN --script-security 2 --config ./teacher-gr3.ovpn 2>&1 >/dev/null &
    sudo $OPENVPN_BIN --script-security 2 --config ./teacher-gr4.ovpn 2>&1 >/dev/null &
    sudo $OPENVPN_BIN --script-security 2 --config ./teacher-gr5.ovpn 2>&1 >/dev/null &
else
    echo "Démarrage de la configuration OpenVPN spécifiée : ${1:-teacher-gr1}"
    sudo $OPENVPN_BIN --script-security 2 --config ./${1:-teacher-gr1}.ovpn
fi