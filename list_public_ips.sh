#!/bin/bash

echo "🔍 Récupération de toutes les IPs publiques AWS..."
echo "-------------------------------------------------"

# Liste les Elastic IPs qui ne sont pas utilisées par un NAT Gateway
echo "🌐 Elastic IPs (EIP) (Non attachées à un NAT Gateway) :"
aws ec2 describe-addresses --query "Addresses[?AssociationId==null].PublicIp" --output text | tr '\n' ' '
echo ""

# Liste les IPs publiques des instances EC2
echo "💻 Instances EC2 (uniquement celles avec une IP publique) :"
aws ec2 describe-instances --filters "Name=instance-state-name,Values=running,stopped" \
    --query "Reservations[*].Instances[?PublicIpAddress!=null].[PublicIpAddress]" --output text | tr '\n' ' '
echo ""

# Liste les IPs publiques des NAT Gateways
echo "🛠 NAT Gateways : (Peuvent réutiliser des Elastic IPs)"
aws ec2 describe-nat-gateways --query "NatGateways[*].NatGatewayAddresses[*].PublicIp" --output text | tr '\n' ' '
echo ""

# Liste les noms DNS des Load Balancers
echo "⚖️  Load Balancers (AWS fournit des DNS, pas d'IPs fixes) :"
aws elb describe-load-balancers --query "LoadBalancerDescriptions[*].DNSName" --output text | tr '\n' ' '
aws elbv2 describe-load-balancers --query "LoadBalancers[*].DNSName" --output text | tr '\n' ' '
echo ""

# Liste les noms DNS des distributions CloudFront
echo "🚀 CloudFront Distributions (Aucune IP fixe, utiliser le DNS) :"
aws cloudfront list-distributions --query "DistributionList.Items[*].DomainName" --output text | tr '\n' ' '
echo ""

echo "✅ Vérification terminée !"
