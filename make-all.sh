#! /bin/bash

export TF_LOG=DEBUG
export TF_LOG_PATH=./terraform.log
> ${TF_LOG_PATH}

test "$1" != "gr1" && ./convert-instances-gr.sh
./easy-rsa.sh
terraform apply -auto-approve -var-file="main.private.tfvars"
sleep 30
./gen-ovpn-configs.sh
./gen-ovpn-clients_configs.sh
