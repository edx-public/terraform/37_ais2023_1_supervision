resource "aws_subnet" "vpc_vpn_gr2" {
  vpc_id            = aws_vpc.vpc_gr2.id
  cidr_block        = "192.168.102.0/27"
  # map_public_ip_on_launch = true

  tags = {
    Name = "vpc_lan_gr2"
  }
}

resource "aws_subnet" "vlan32_gr2" {
  vpc_id            = aws_vpc.vpc_gr2.id
  cidr_block        = "192.168.102.32/28"
  map_public_ip_on_launch = true

  tags = {
    Name = "vlan32_gr2"
  }
}

resource "aws_subnet" "vlan48_gr2" {
  vpc_id            = aws_vpc.vpc_gr2.id
  cidr_block        = "192.168.102.48/28"
  # map_public_ip_on_launch = true

  tags = {
    Name = "vlan48_gr2"
  }
}

resource "aws_subnet" "vlan64_gr2" {
  vpc_id            = aws_vpc.vpc_gr2.id
  cidr_block        = "192.168.102.64/26"

  tags = {
    Name = "vlan64_gr2"
  }
}

resource "aws_subnet" "vlan128_gr2" {
  vpc_id            = aws_vpc.vpc_gr2.id
  cidr_block        = "192.168.102.128/26"

  tags = {
    Name = "vlan128_gr2"
  }
}

resource "aws_subnet" "vlan192_gr2" {
  vpc_id            = aws_vpc.vpc_gr2.id
  cidr_block        = "192.168.102.192/26"

  tags = {
    Name = "vlan192_gr2"
  }
}

resource "aws_internet_gateway" "igw_gr2" {
  vpc_id = aws_vpc.vpc_gr2.id

  tags = {
    Name = "igw_gr2"
  }
}

resource "aws_eip" "mon_eip_nat_gr2" {
  domain = "vpc"
}

resource "aws_nat_gateway" "nat_gateway_vpc_gr2" {
  allocation_id = aws_eip.mon_eip_nat_gr2.id
  subnet_id     = aws_subnet.vlan32_gr2.id
  depends_on    = [aws_internet_gateway.igw_gr2]
}


resource "aws_route_table" "rt_vlan32_gr2" {
  vpc_id = aws_vpc.vpc_gr2.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_gr2.id
  }

  tags = {
    Name = "rt_vlan32_gr2"
  }
}

resource "aws_route_table_association" "rta_vlan32_gr2" {
  subnet_id      = aws_subnet.vlan32_gr2.id
  route_table_id = aws_route_table.rt_vlan32_gr2.id
}

resource "aws_route_table" "rt_private_gr2" {
  vpc_id = aws_vpc.vpc_gr2.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_vpc_gr2.id
  }
}

resource "aws_route_table_association" "rta_vlan48_gr2" {
  subnet_id      = aws_subnet.vlan48_gr2.id
  route_table_id = aws_route_table.rt_private_gr2.id
}

resource "aws_route_table_association" "rta_vlan64_gr2" {
  subnet_id      = aws_subnet.vlan64_gr2.id
  route_table_id = aws_route_table.rt_private_gr2.id
}

resource "aws_route_table_association" "rta_vlan128_gr2" {
  subnet_id      = aws_subnet.vlan128_gr2.id
  route_table_id = aws_route_table.rt_private_gr2.id
}

resource "aws_route_table_association" "rta_vlan192_gr2" {
  subnet_id      = aws_subnet.vlan192_gr2.id
  route_table_id = aws_route_table.rt_private_gr2.id
}