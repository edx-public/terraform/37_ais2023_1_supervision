resource "aws_subnet" "vpc_vpn_gr5" {
  vpc_id            = aws_vpc.vpc_gr5.id
  cidr_block        = "192.168.105.0/27"
  # map_public_ip_on_launch = true

  tags = {
    Name = "vpc_lan_gr5"
  }
}

resource "aws_subnet" "vlan32_gr5" {
  vpc_id            = aws_vpc.vpc_gr5.id
  cidr_block        = "192.168.105.32/28"
  map_public_ip_on_launch = true

  tags = {
    Name = "vlan32_gr5"
  }
}

resource "aws_subnet" "vlan48_gr5" {
  vpc_id            = aws_vpc.vpc_gr5.id
  cidr_block        = "192.168.105.48/28"
  # map_public_ip_on_launch = true

  tags = {
    Name = "vlan48_gr5"
  }
}

resource "aws_subnet" "vlan64_gr5" {
  vpc_id            = aws_vpc.vpc_gr5.id
  cidr_block        = "192.168.105.64/26"

  tags = {
    Name = "vlan64_gr5"
  }
}

resource "aws_subnet" "vlan128_gr5" {
  vpc_id            = aws_vpc.vpc_gr5.id
  cidr_block        = "192.168.105.128/26"

  tags = {
    Name = "vlan128_gr5"
  }
}

resource "aws_subnet" "vlan192_gr5" {
  vpc_id            = aws_vpc.vpc_gr5.id
  cidr_block        = "192.168.105.192/26"

  tags = {
    Name = "vlan192_gr5"
  }
}

resource "aws_internet_gateway" "igw_gr5" {
  vpc_id = aws_vpc.vpc_gr5.id

  tags = {
    Name = "igw_gr5"
  }
}

resource "aws_eip" "mon_eip_nat_gr5" {
  domain = "vpc"
}

resource "aws_nat_gateway" "nat_gateway_vpc_gr5" {
  allocation_id = aws_eip.mon_eip_nat_gr5.id
  subnet_id     = aws_subnet.vlan32_gr5.id
  depends_on    = [aws_internet_gateway.igw_gr5]
}


resource "aws_route_table" "rt_vlan32_gr5" {
  vpc_id = aws_vpc.vpc_gr5.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_gr5.id
  }

  tags = {
    Name = "rt_vlan32_gr5"
  }
}

resource "aws_route_table_association" "rta_vlan32_gr5" {
  subnet_id      = aws_subnet.vlan32_gr5.id
  route_table_id = aws_route_table.rt_vlan32_gr5.id
}

resource "aws_route_table" "rt_private_gr5" {
  vpc_id = aws_vpc.vpc_gr5.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_vpc_gr5.id
  }
}

resource "aws_route_table_association" "rta_vlan48_gr5" {
  subnet_id      = aws_subnet.vlan48_gr5.id
  route_table_id = aws_route_table.rt_private_gr5.id
}

resource "aws_route_table_association" "rta_vlan64_gr5" {
  subnet_id      = aws_subnet.vlan64_gr5.id
  route_table_id = aws_route_table.rt_private_gr5.id
}

resource "aws_route_table_association" "rta_vlan128_gr5" {
  subnet_id      = aws_subnet.vlan128_gr5.id
  route_table_id = aws_route_table.rt_private_gr5.id
}

resource "aws_route_table_association" "rta_vlan192_gr5" {
  subnet_id      = aws_subnet.vlan192_gr5.id
  route_table_id = aws_route_table.rt_private_gr5.id
}