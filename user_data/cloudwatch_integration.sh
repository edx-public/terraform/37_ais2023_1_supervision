#!/bin/bash
yum update -y
yum install -y amazon-cloudwatch-agent

#!/bin/bash

# Assurez-vous que le répertoire de configuration existe
mkdir -p /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.d
mkdir -p /var/lib/amazon-cloudwatch-agent/logs

# Configuration de l'agent CloudWatch pour collecter des métriques et des logs
cat > /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json <<EOL
{
  "agent": {
    "metrics_collection_interval": 60,
    "logfile": "/opt/aws/amazon-cloudwatch-agent/logs/amazon-cloudwatch-agent.log"
  },
  "logs": {
    "logs_collected": {
      "files": {
        "collect_list": [
          {
            "file_path": "/opt/aws/amazon-cloudwatch-agent/logs/amazon-cloudwatch-agent.log",
            "log_group_name": "/ec2/37_AIS2024_1",
            "log_stream_name": "{instance_id}",
            "timezone": "Local"
          }
        ]
      }
    },
    "force_flush_interval": 5
  },
  "metrics": {
    "append_dimensions": {
      "InstanceId": "\${aws:InstanceId}"
    },
    "metrics_collected": {
      "mem": {
        "measurement": [
          "mem_used_percent"
        ],
        "metrics_collection_interval": 60
      },
      "disk": {
        "measurement": [
          "disk_used_percent"
        ],
        "metrics_collection_interval": 60,
        "resources": [
          "/"
        ]
      },
      "swap": {
        "measurement": [
          "swap_used_percent"
        ],
        "metrics_collection_interval": 60
      },
      "cpu": {
        "resources": [
          "*"
        ],
        "totalcpu": false,
        "measurement": [
          "cpu_usage_idle",
          "cpu_usage_iowait",
          "cpu_usage_user",
          "cpu_usage_system"
        ],
        "metrics_collection_interval": 60
      }
    }
  }
}
EOL

# Configuration pour la collecte de logs système via systemd
cat > /opt/aws/amazon-cloudwatch-agent/etc/common-config.toml <<EOL
[inputs.logfile]
destination = "cloudwatchlogs"
log_group_name = "/ec2/37_AIS2024_1"
log_stream_name = "systemd-logs"
file_state_folder = "/var/lib/amazon-cloudwatch-agent/logs"
auto_fill_log_group_name = true
auto_fill_log_stream_name = true
pipe = true
log_stream_name_prefix = "systemd"
EOL

# Démarrer l'agent CloudWatch
/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl \
    -a fetch-config \
    -m ec2 \
    -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json \
    -s