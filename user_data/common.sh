#!/bin/bash

# Mettre à jour le système
echo "Mise à jour"
apt-get update -y
apt-get install --assume-yes htop tree mc vim tmux git

# URL du fichier .bashrc à télécharger
BASHRC_URL="https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/config_files/bashrc?ref_type=heads"

# Télécharge le nouveau .bashrc pour root
curl -s "$BASHRC_URL" -o /root/.bashrc

# Assure que root est le propriétaire et définit les bons droits
chown root:root /root/.bashrc
chmod 644 /root/.bashrc

# Répète l'opération pour l'utilisateur admin
curl -s "$BASHRC_URL" -o /home/admin/.bashrc
chown admin:admin /home/admin/.bashrc
chmod 644 /home/admin/.bashrc

git clone https://github.com/gpakosz/.tmux.git /usr/local/etc/tmux-config
# Nouvel emplacement du dépôt cloné
TMUX_CONFIG_DIR="/usr/local/etc/tmux-config"

# Boucle à travers chaque répertoire utilisateur dans /home
for user_dir in /home/*; do
  if [ -d "$user_dir" ]; then
    username=$(basename "$user_dir")

    # Crée un lien symbolique pour .tmux.conf
    ln -sf "$TMUX_CONFIG_DIR/.tmux.conf" "$user_dir/.tmux.conf"
    
    # Copie .tmux.conf.local s'il n'existe pas
    if [ ! -f "$user_dir/.tmux.conf.local" ]; then
      cp "$TMUX_CONFIG_DIR/.tmux.conf.local" "$user_dir/.tmux.conf.local"
      chown "$username":"$username" "$user_dir/.tmux.conf.local"
    fi

    echo "Configuration tmux mise à jour pour l'utilisateur $username"
  fi
done

# Applique également la configuration pour l'utilisateur root si nécessaire
ln -sf "$TMUX_CONFIG_DIR/.tmux.conf" /root/.tmux.conf
if [ ! -f "/root/.tmux.conf.local" ]; then
  cp "$TMUX_CONFIG_DIR/.tmux.conf.local" /root/.tmux.conf.local
fi