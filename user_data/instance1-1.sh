#!/bin/bash


# Télécharger et exécuter Pi-hole
echo "Téléchargement des containers ..."
docker pull pihole/pihole:latest
# docker pull ubuntu/squid:5.2-22.04_beta

# cat > /etc/apt/apt.conf.d/00proxy <<EOF
# Acquire::http::Proxy "http://192.168.10?.55:3128";
# Acquire::https::Proxy "http://192.168.10?.55:3128";
# EOF

# docker run -d --name squid -e TZ=Europe/Paris -p 3128:3128 ubuntu/squid:5.2-22.04_beta

# Sauvegarder le fichier de configuration original
sudo cp /etc/systemd/resolved.conf /etc/systemd/resolved.conf.backup

# Désactiver l'écoute du port 53 par systemd-resolved
echo "DNSStubListener=no" | sudo tee -a /etc/systemd/resolved.conf

# Redémarrer systemd-resolved
sudo systemctl restart systemd-resolved

# Vérifier que le port 53 est libre (optionnel)
# echo "Vérification des ports en écoute :"
# sudo ss -tulwn | grep ':53 '

# Exécuter le conteneur Docker Pi-hole
docker run -d --name pihole --restart=unless-stopped \
  --net=host \
  -e WEBPASSWORD='MOT_DE_PASSE_ADMIN' \
  --hostname=pi.hole \
  --cap-add=NET_ADMIN --dns=1.1.1.1 \
  pihole/pihole:latest

