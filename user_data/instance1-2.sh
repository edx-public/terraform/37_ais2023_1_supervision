#! /bin/bash
# https://hub.docker.com/r/ealen/echo-server
# https://ealenn.github.io/Echo-Server/pages/quick-start/docker.html
# https://docs.datalust.co/docs/getting-started-with-docker

# curl http://192.168.10?.56:3000/\?echo_body\=amazing

IP_ADDR=$(ip addr show docker0 | grep 'inet' | awk '{print $2}' | cut -d/ -f1 | grep -v "fe80")

docker run -d \
    -p 3000:80 \
    -e LOGS__SEQ__ENABLED=true \
    -e LOGS__SEQ__SERVER=http://${IP_ADDR}:5341 \
    ealen/echo-server:latest

docker run -d \
    -p 80:80 \
    -p 5341:5341 \
    -e ACCEPT_EULA=Y \
    datalust/seq:latest