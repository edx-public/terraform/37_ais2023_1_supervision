#!/bin/bash

# Mettre à jour la liste des paquets et installer le serveur NFS
apt update -y
apt install --assume-yes nfs-kernel-server

# Créer le répertoire à partager (exemple : /var/nfs/share, modifiez selon vos besoins)
mkdir -p /var/nfs/share
chown nobody:nogroup /var/nfs/share

# Donner des permissions de lecture/écriture (modifiez selon vos besoins)
chmod 777 /var/nfs/share

# Ajouter la configuration d'exportation NFS (ajoutez votre propre réseau ou IP à la place de 192.168.100.0/21)
echo "/var/nfs/share 192.168.100.0/21(rw,sync,no_root_squash,no_subtree_check)" >> /etc/exports

# Exporter les partages configurés
exportfs -a

# Redémarrer le service NFS pour appliquer la configuration
systemctl restart nfs-kernel-server

# Mettre à jour les règles du pare-feu pour autoriser le trafic NFS (ajustez selon votre configuration de pare-feu)
# ufw allow from 192.168.100.0/21 to any port nfs
# ufw enable
# ufw status
