#!/bin/bash

# Nom de l'image et du conteneur
IMAGE_NAME="monserveurhttps"
CONTAINER_NAME="nginx_https"

# Création du Dockerfile
cat > Dockerfile <<EOF
FROM nginx:alpine
RUN apk add --no-cache openssl \\
    && openssl req -x509 -nodes -days 365 -newkey rsa:2048 \\
        -keyout /etc/ssl/private/nginx-selfsigned.key \\
        -out /etc/ssl/certs/nginx-selfsigned.crt \\
        -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=instance3-1.aws.cefim.intra" \\
    && chmod 600 /etc/ssl/private/nginx-selfsigned.key /etc/ssl/certs/nginx-selfsigned.crt
EOF

# Création du fichier de configuration Nginx, incluant la page de statut
cat > default.conf <<EOF
server {
    listen 443 ssl;
    server_name instance3-1.aws.cefim.intra;

    ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
    ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;

    location / {
        root /usr/share/nginx/html;
        index index.html;
    }

    # Configuration pour la page de statut
    location /basic_status {
        stub_status on;
        allow 127.0.0.1;  # Autoriser l'accès local uniquement
        deny all;         # Refuser les autres accès
    }
}
EOF

# Création d'un site d'exemple sur le thème de la métallurgie
cat > index.html <<EOF
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Galactic Getaways: Voyages Intergalactiques</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #0A0A23;
            color: #D6D6F6;
            text-align: center;
            padding: 20px;
        }
        h1 {
            color: #FFD700;
        }
        p {
            margin: 20px auto;
            width: 90%;
            max-width: 600px;
        }
    </style>
</head>
<body>
    <h1>Galactic Getaways - Le comité d'entreprise de BELINOX</h1>
    <p>Envie de quitter la Terre pour les vacances ? Marre des plages bondées et des montagnes surpeuplées ?</p>
    <p>Venez chez <strong>Galactic Getaways</strong>, votre agence de voyage intergalactique numéro un dans la Voie lactée !</p>
    <p>🚀 Offres exclusives :</p>
    <ul>
        <li><strong>Week-end sur la Lune</strong> : parfait pour les débutants en gravité réduite.</li>
        <li><strong>Retraite spirituelle sur Jupiter</strong> : flottez dans la grandeur de ses nuages (attention, séjour dans une station spatiale, Jupiter étant une géante gazeuse).</li>
        <li><strong>Voyage de noces sur Titan</strong> : des lacs d'hydrocarbures pour une romance inflammable.</li>
        <li><strong>Excursion d'un jour dans un Trou Noir</strong> : le temps s'arrête, littéralement (clause de non-responsabilité pour les effets relativistes).</li>
    </ul>
    <p>🌌 Pourquoi choisir Galactic Getaways ?</p>
    <p>Parce que chez nous, la <em>"distanciation sociale"</em> prend tout son sens. Plusieurs milliards de kilomètres garantissent que vous n'aurez à partager votre espace avec personne d'autre.</p>
    <p>Contactez-nous via votre télépathe local pour réserver le voyage de vos rêves. Et rappelez-vous : l'espace, c'est grand, mais notre service client est encore plus vaste !</p>
</body>
</html>

EOF

# Construction de l'image Docker
docker build -t $IMAGE_NAME -f- . <<EOF
FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/default.conf
COPY index.html /usr/share/nginx/html/index.html
RUN apk add --no-cache openssl \\
    && openssl req -x509 -nodes -days 365 -newkey rsa:2048 \\
        -keyout /etc/ssl/private/nginx-selfsigned.key \\
        -out /etc/ssl/certs/nginx-selfsigned.crt \\
        -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=instance3-1.aws.cefim.intra" \\
    && chmod 600 /etc/ssl/private/nginx-selfsigned.key /etc/ssl/certs/nginx-selfsigned.crt
EOF

# Vérification si le conteneur existe déjà et le supprime si c'est le cas
if docker ps -a | grep -q $CONTAINER_NAME; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

# Démarrage du conteneur avec la stratégie de redémarrage
docker run -d \
    --name $CONTAINER_NAME \
    -p 8443:443 \
    --restart unless-stopped \
    $IMAGE_NAME
