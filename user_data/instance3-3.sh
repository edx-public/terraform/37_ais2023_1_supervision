#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

# Mise à jour du système
apt update && apt upgrade -y

# Mise à jour de la liste des locales disponibles
# dpkg-reconfigure -f noninteractive locales
# locale-gen fr_FR.UTF-8
# update-locale en_US.UTF-8


# Installation de MariaDB (MySQL)
apt install -y mariadb-server mariadb-client

# Sécurisation de MariaDB et création de la base de données Zabbix
mysql -e "CREATE DATABASE zabbix character set utf8mb4 collate utf8mb4_bin;"
mysql -e "CREATE USER 'zabbix'@'localhost' IDENTIFIED BY 'your_zabbix_password';"
mysql -e "GRANT ALL PRIVILEGES ON zabbix.* TO 'zabbix'@'localhost';"
mysql -e "set global log_bin_trust_function_creators = 1;"
mysql -e "FLUSH PRIVILEGES;"

# Ajout du dépôt Zabbix officiel
wget https://repo.zabbix.com/zabbix/6.4/debian/pool/main/z/zabbix-release/zabbix-release_6.4-1+debian12_all.deb
dpkg -i zabbix-release_6.4-1+debian12_all.deb
apt update 

# Installation du serveur Zabbix, de l'agent et du frontend
apt-get install -y zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-sql-scripts zabbix-agent

# Configuration de la base de données pour Zabbix
zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -p'your_zabbix_password' zabbix

# Configuration du fichier Zabbix Server pour utiliser la base de données
sed -i "s/# DBPassword=/DBPassword=your_zabbix_password/" /etc/zabbix/zabbix_server.conf

# Démarrage des services et activation au démarrage
systemctl restart zabbix-server zabbix-agent apache2
systemctl enable zabbix-server zabbix-agent apache2

# Configuration de la timezone PHP pour Zabbix
# php_timezone=$(cat /etc/timezone)
# sed -i "s|# php_value date.timezone Europe/Paris|php_value date.timezone ${php_timezone}|" /etc/zabbix/apache.conf

# Redémarrage d'Apache pour appliquer les changements
# systemctl restart apache2
