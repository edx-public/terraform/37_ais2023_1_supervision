#!/bin/bash

# Définition des variables
ICECAST_PASSWORD="Icecast123$"
ICECAST_SOURCE_PASSWORD="source123$"
ICECAST_RELAY_PASSWORD="relay123$"
ICECAST_ADMIN_PASSWORD="admin123$"

# Téléchargement du fichier OGG
wget -O Symphony.ogg "https://filesamples.com/samples/audio/ogg/Symphony%20No.6%20(1st%20movement).ogg"
wget -O ices-playlist.xml "https://framagit.org/edx-public/terraform/37_ais2024_1_supervision/-/raw/main/config_files/ices-playlist.xml?ref_type=heads"
install -d -m 777 ./logs

echo "/var/music/Symphony.ogg" > playlist.txt

cat > supervisord.conf <<EOF
[supervisord]
nodaemon=true

[program:icecast]
command=icecast2 -c /etc/icecast2/icecast.xml
autostart=true
autorestart=true

[program:ices]
command=bash -c 'sleep 15 && ices2 /var/music/ices-playlist.xml'
autostart=true
autorestart=true
EOF

# Génération du fichier de configuration icecast.xml
cat > icecast.xml <<EOF
<icecast>
    <limits>
        <clients>100</clients>
        <sources>2</sources>
    </limits>

    <authentication>
        <source-password>$ICECAST_SOURCE_PASSWORD</source-password>
        <relay-password>$ICECAST_RELAY_PASSWORD</relay-password>
        <admin-user>admin</admin-user>
        <admin-password>$ICECAST_ADMIN_PASSWORD</admin-password>
    </authentication>

    <listen-socket>
        <port>8000</port>
    </listen-socket>

    <hostname>localhost</hostname>

    <paths>
        <logdir>/var/log/icecast</logdir>
        <webroot>/usr/share/icecast/web</webroot>
        <adminroot>/usr/share/icecast/admin</adminroot>
        <alias source="/" destination="/status.xsl"/>
    </paths>

    <logging>
        <accesslog>access.log</accesslog>
        <errorlog>error.log</errorlog>
        <loglevel>3</loglevel> <!-- 4 Debug, 3 Info, 2 Warn, 1 Error -->
    </logging>

    <security>
        # <chroot>0</chroot>
        <changeowner>
            <user>nobody</user>
            <group>nogroup</group>
        </changeowner>
    </security>

    <mount>
        <mount-name>/mountpoint</mount-name>
        <password>$ICECAST_PASSWORD</password>
    </mount>
</icecast>

EOF

# Création du Dockerfile
cat > Dockerfile <<EOF
FROM ubuntu:latest
RUN apt-get update && apt-get install -y icecast2 ices2 supervisor
COPY Symphony.ogg /var/music/Symphony.ogg
COPY playlist.txt /var/music/playlist.txt
COPY ices-playlist.xml /var/music/ices-playlist.xml
COPY icecast.xml /etc/icecast2/icecast.xml
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
EXPOSE 8000
CMD ["/usr/bin/supervisord"]
# CMD icecast2 -c /etc/icecast2/icecast.xml & ices2 /var/music/ices-playlist.xml
EOF

docker build -t icecast-server .
docker run -d --name icecast-server -p 8000:8000 -v ./logs:/var/log/icecast --restart unless-stopped icecast-server

