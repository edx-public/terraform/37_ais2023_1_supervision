#!/bin/bash
# curl http://localhost:9090/-/healthy


# Définir le répertoire de travail
WORKDIR="/opt/grafana-docker"
mkdir -p "$WORKDIR"

# Naviguer dans le répertoire de travail
cd "$WORKDIR"

# Étape 1: Créer le fichier docker-compose.yml
cat <<EOF > docker-compose.yml
version: '3'
services:
  grafana:
    image: grafana/grafana:latest
    ports:
      - "3000:3000"
    environment:
      GF_SECURITY_ADMIN_PASSWORD: "Cefim123$"
      GF_USERS_ALLOW_SIGN_UP: "false"
    depends_on:
      - prometheus

  prometheus:
    image: prom/prometheus:latest
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - "9090:9090"
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'

  node_exporter:
    image: prom/node-exporter:latest
    volumes:
      - "/proc:/host/proc:ro"
      - "/sys:/host/sys:ro"
      - "/:/rootfs:ro"
    command:
      - '--path.procfs=/host/proc'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.ignored-mount-points'
      - '^/(sys|proc|dev|host|etc)($$|/)'
    ports:
      - "9100:9100"
    privileged: true
    restart: unless-stopped
EOF

# Étape 2: Créer le fichier de configuration prometheus.yml
cat <<EOF > prometheus.yml
global:
  scrape_interval: 15s

scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['localhost:9090']
  - job_name: 'node'
    static_configs:
      - targets: ['node_exporter:9100']
EOF

# Étape 3: Lancer Docker Compose
docker compose up -d

# Étape 4: Configuration automatique de Grafana
# configure_grafana() {
#   GRAFANA_URL="http://admin:Cefim123$@localhost:3000"

#   # Attendre que Grafana soit prêt
#   echo "Attente de Grafana..."
#   until $(curl --output /dev/null --silent --head --fail "$GRAFANA_URL"); do
#     printf '.'
#     sleep 5
#   done

#   echo "Grafana est prêt. Configuration de la source de données Prometheus..."

#   # Configurer Prometheus comme source de données
#   curl -s -X POST "$GRAFANA_URL/api/datasources" -H "Content-Type: application/json" \
#       -d '{
#             "name": "Prometheus",
#             "type": "prometheus",
#             "url": "http://prometheus:9090",
#             "access": "proxy",
#             "isDefault": true
#           }'
# }

# configure_grafana
