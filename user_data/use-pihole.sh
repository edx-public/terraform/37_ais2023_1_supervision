#!/bin/bash

# Obtient l'adresse de la passerelle par défaut
DEFAULT_GATEWAY=$(ip route | grep default | awk '{print $3}')

# Extrait le préfixe de l'adresse de la passerelle
GATEWAY_PREFIX=$(echo $DEFAULT_GATEWAY | cut -d. -f3)

case "$DEFAULT_GATEWAY" in
    192.168.101.*) DNS_SERVER="192.168.101.55" ;;
    192.168.102.*) DNS_SERVER="192.168.102.55" ;;
    192.168.103.*) DNS_SERVER="192.168.103.55" ;;
    192.168.104.*) DNS_SERVER="192.168.104.55" ;;
    192.168.105.*) DNS_SERVER="192.168.105.55" ;;
    *) echo "La passerelle par défaut ne correspond à aucun préfixe configuré." ; exit 1 ;;
esac


# Affiche le serveur DNS sélectionné
echo "Configuration du serveur DNS sur $DNS_SERVER basé sur la passerelle par défaut $DEFAULT_GATEWAY."

# Sauvegarde le fichier de configuration original (au cas où)
sudo cp /etc/systemd/resolved.conf /etc/systemd/resolved.conf.backup

# Ajoute ou modifie la ligne DNS dans /etc/systemd/resolved.conf
sudo sed -i "/^DNS=/c\DNS=$DNS_SERVER" /etc/systemd/resolved.conf
if ! grep -q "^DNS=" /etc/systemd/resolved.conf; then
  echo "DNS=$DNS_SERVER" | sudo tee -a /etc/systemd/resolved.conf > /dev/null
fi

# Redémarre systemd-resolved pour appliquer les changements
sudo systemctl restart systemd-resolved

echo "Le service systemd-resolved a été redémarré avec le serveur DNS configuré sur $DNS_SERVER."
