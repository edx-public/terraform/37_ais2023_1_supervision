variable "ca_certificate_arn" {
  description = "ARN de la CA"
  type        = string
}

variable "server_gr1_certificate_arn" {
  description = "ARN du certificat serveur pour le Client VPN Endpoint"
  type        = string
}

variable "server_gr2_certificate_arn" {
  description = "ARN du certificat serveur pour le Client VPN Endpoint"
  type        = string
}

variable "server_gr3_certificate_arn" {
  description = "ARN du certificat serveur pour le Client VPN Endpoint"
  type        = string
}

variable "server_gr4_certificate_arn" {
  description = "ARN du certificat serveur pour le Client VPN Endpoint"
  type        = string
}

variable "server_gr5_certificate_arn" {
  description = "ARN du certificat serveur pour le Client VPN Endpoint"
  type        = string
}

variable "client1_gr1_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client2_gr1_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client3_gr1_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client4_gr1_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client5_gr1_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client1_gr2_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client2_gr2_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client3_gr2_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client4_gr2_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client5_gr2_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client1_gr3_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client2_gr3_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client3_gr3_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client4_gr3_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client5_gr3_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client1_gr4_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client2_gr4_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client3_gr4_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client4_gr4_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client5_gr4_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client1_gr5_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client2_gr5_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client3_gr5_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client4_gr5_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}

variable "client5_gr5_certificate_arn" {
  description = "ARN du certificat client1 pour le Client VPN Endpoint"
  type        = string
}