resource "aws_vpc" "vpc_gr1" {
  cidr_block = "192.168.101.0/24"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "vpc_gr1"
  }
}

resource "aws_vpc" "vpc_gr2" {
  cidr_block = "192.168.102.0/24"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "vpc_gr2"
  }
}

resource "aws_vpc" "vpc_gr3" {
  cidr_block = "192.168.103.0/24"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "vpc_gr3"
  }
}

resource "aws_vpc" "vpc_gr4" {
  cidr_block = "192.168.104.0/24"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "vpc_gr4"
  }
}

resource "aws_vpc" "vpc_gr5" {
  cidr_block = "192.168.105.0/24"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "vpc_gr5"
  }
}