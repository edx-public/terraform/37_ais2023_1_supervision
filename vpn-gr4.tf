resource "aws_ec2_client_vpn_endpoint" "vpn_gr4" {
  # depends_on             = [ aws_eip_association.eip_assoc_gr4 ]
  description            = "VPN Client Endpoint"
  server_certificate_arn = var.server_gr4_certificate_arn
  client_cidr_block      = "10.4.0.0/16"
  vpc_id                 = aws_vpc.vpc_gr4.id
  
  split_tunnel           = true

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = var.ca_certificate_arn
  }

  connection_log_options {
    enabled = false
  }

  transport_protocol     = "udp"

}
# aws ec2 describe-client-vpn-endpoints

resource "aws_ec2_client_vpn_network_association" "subnet_assoc_gr4" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn_gr4.id
  subnet_id              = aws_subnet.vpc_vpn_gr4.id
  lifecycle {
    ignore_changes = [subnet_id] # This is a hack to fix a bug: https://github.com/terraform-providers/terraform-provider-aws/issues/7597
  }
}

resource "aws_ec2_client_vpn_authorization_rule" "vpn_access_gr4" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn_gr4.id
  target_network_cidr    = "192.168.104.0/24"
  authorize_all_groups   = true
}
